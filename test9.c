#include <stdio.h>
int main ()
{
  int a,b,*x,*y,n;
  printf("enter the 2 numbers:\n");
  scanf("%d %d",&a,&b);
  printf("before swapping:\n a=%d\n b=%d\n",a,b);
  x= &a;
  y= &b;
  n=*y;
  *y=*x;
  *x=n;
 printf("after swapping:\n a=%d\n b=%d",a,b);
 return 0;
}
